/*==============================================================================
  # Woocommerce
==============================================================================*/
(function ($) {
    
    // SEARCH INPUT TOGGLE
    $(".search-wrapper").click(function() {
        $(this).toggleClass("active");
        $(".search-result-wrapper").toggleClass("active");
        $(".meny-wrapper").addClass("andSearch");
    });
    
    // SEARCH INPUT TOGGLE
    $(".mobile-wrapper .input-wrapper").click(function() {
        $(this).toggleClass("active");
        $(".search-result-wrapper").toggleClass("active");
    });
    
    // BURGER TOGGLE
    $(".burger-wrapper").click(function() {
      $(this).toggleClass("active");
      $(".meny-wrapper").toggleClass("active");
      $(".search-result-wrapper").removeClass("active");
      $(".meny-wrapper").removeClass("andSearch");
    });

    // APPEND SPINNER
    $(".wpcf7-submit").click(function(){
      $('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>').appendTo(".ajax-loader");
    });

    //KVMM from frame
    $(".wccpf-field[name^='frame_outer_']").change(() => {
      var width = $(".wccpf-field[name^='frame_outer_width']").val() || 0;
      var height = $(".wccpf-field[name^='frame_outer_height']").val() || 0;

      $("input[name^='frame_kvmm']").val(width * height);
      
    });

    
  })(jQuery);
  

