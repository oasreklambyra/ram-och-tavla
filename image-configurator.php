<?php
	
	/* Source: https://github.com/shpontex/cropme */
	/* enqueue style and script */
	add_action( 'woocommerce_before_single_product', 'crompme_enqueue' );

	function crompme_enqueue() {
		
		wp_enqueue_script( 'croppie_js', get_template_directory_uri() . '/assets/vendor/croppie/croppie.min.js', array('jquery'));
		wp_enqueue_style( 'croppie_css', get_template_directory_uri() . '/assets/vendor/croppie/croppie.css');

		wp_enqueue_script( 'html2canvas_js', get_template_directory_uri() . '/assets/vendor/dtm/dom-to-image-more.min.js', array('jquery'));
	}

	add_action( 'woocommerce_before_add_to_cart_button', 'add_crompme_to_product_html', 15 );

	function add_crompme_to_product_html() {
		$path = get_field('frame');
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$border_raw = file_get_contents($path);
		$border = 'data:image/' . $type . ';base64,' . base64_encode($border_raw);
		?> 

		<style type="text/css">
			.wrapper-inner {
				background-color: #f7f8fa;
			}

			.hidden-wrapper, .hidden {
				display: none !important;
			}

			.wccpf-fields-container .wccpf_label {
				width: 215px !important;
			}

			.wccpf-fields-container .wccpf_label label {
				width: 215px !important;
			}


			.cut-inner {
				background-color: #fff;
				padding: 2rem 0;
				margin-top: 1rem;
				overflow: hidden;
			}

			.croppie-container {
				height: auto !important;
			}

			.croppie-container .cr-boundary {
				overflow: unset !important;
			}

			.croppie-container .cr-viewport {
				box-shadow: 0 0 2000px 2000px rgba(0, 0, 0, 0.2) !important;
			}

			.croppie-container .cr-image {
				height: auto !important;
				width: auto !important;
			}
			
			#canvas-wrapper {
				margin: 2rem auto;
			}



			#canvas-border, #canvas-border-hidden {
				border: 15px solid #fff;
				border-image: url("<?php echo $border ?>");
				border-image-repeat: stretch;
    			border-image-slice: 20;
    			position: relative;
    			margin: 0 auto;
    			box-shadow: 0 0 2000px 2000px rgba(0, 0, 0, 0.2);
    			background-color: #fff;
			}

			#canvas-border-hidden {
				margin: 0;
				position: absolute;
				top: 0;
				left: 0;
				z-index: -1;
				box-shadow: none;
			}

			#canvas, #canvas-hidden {
				background-size: cover;
			    height: 100%;
			    width: 100%;
			    background-position: center center;
			    background-repeat: no-repeat;
			    background-color: #ffffff;
			}

			#canvas-hidden {
				margin: 15px;
				width: calc(100% - 30px);
				height: calc(100% - 30px)
			}
			
			
			#canvas-passepartout {
				position: absolute;
				top: 0;
				left:0;
				right:0;
				bottom:0;
				width:100%;
				height:100%;
				border: solid 0px transparent;
			}

			.single_add_to_cart_button.loading-cut {
				position: relative;
			}

			.single_add_to_cart_button.loading-cut:before {
				position: absolute;
				content: ' ';
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
				background-color: #fff;
				opacity: 0.5;
			}
			
		</style>
		
		<?php
	}

	add_action( 'woocommerce_before_add_to_cart_button', 'add_crompme_to_product_js' );

	function add_crompme_to_product_js() {
		?>

		<script>
			jQuery(document).ready(function($) {
				var croppie = null
				var wrapperWidth = $('.woocommerce-product-gallery').width()/1.5

				var image = null
				var pxWidth = null
				var pxHeight = null
				var dpi = 300

				var maxWidth = null
				var maxHeight = null

				var frameInnerWidth = null
				var frameOuterWidth = null
 				var	frameInnerHeight = null 
 				var frameOuterHeight = null

 				var passepartoutTop = 0
 				var passepartoutBottom = 0
 				var passepartoutLeft = 0
 				var passepartoutRight = 0
 				var passepartoutColor = '#ffffff'

 				function init() {
					$('.woocommerce-product-gallery')
						.append(`
							<div id="cropping-wrapper">
								<div id="croppie-wrapper" class="hidden">
									<div>Beskär bilden till er preferens</div>
									<div class="cut-inner">
										<div id="croppie">
										</div>
									</div>
								</div>
								<div id="canvas-wrapper" class="hidden">
									<div>Detta är en förhandsvisning av hur färdigt resultat kommer seut</div>
									<div class="cut-inner">
										<div class="margin-wrapper">
											<div id="canvas-border">
												<div id="canvas-passepartout"></div>
												<img id="canvas">
											</div>

											<div id="canvas-border-hidden">
												<img id="canvas-hidden">
											</div>
										</div>
									</div>
								</div>


							</div>`
						)
					setArea()
 				}

 				init()

 				
 				function replaceGallery() {
 					$('.woocommerce-product-gallery>*:not(#cropping-wrapper)').fadeOut(300, () => {

 					})
 				}

 				function loadCrop() {
 					var input = $(".js-custom-image input")[0]
						
					console.log(input.files, frameInnerWidth, frameInnerHeight)
					
 					if (input && input.files && input.files[0] && frameInnerWidth && frameInnerHeight) {
						
 						var zoom = wrapperWidth/frameInnerWidth
		    			var width = wrapperWidth
		    			var height = frameInnerHeight * zoom

			    		var reader = new FileReader();
			    		
			    		reader.onload = function(){
			    			if(croppie) {
				    			$('#croppie').croppie('destroy')
				    		}

			    			croppie = $('#croppie').croppie({
							    url: reader.result,
							    boundary: { width: width, height: height },
							    viewport: { width: width, height: height },
							    showZoomer: false
							});

							$('#croppie-wrapper').removeClass('hidden')
							loadPreview()
			            }
		 
		 	        	reader.readAsDataURL(input.files[0]);

		 	        	
 					}
 				}

 				function loadPreview () {
 					if(croppie) {
 						var zoom = wrapperWidth/frameInnerWidth
		    			var width = wrapperWidth
		    			var height = frameInnerHeight * zoom

	 					$('#canvas-wrapper').removeClass('hidden')
						$('#canvas-border').css('width', width + 'px')
						$('#canvas-border').css('height', height + 'px')

						$('#canvas-passepartout').css('border-top-width', passepartoutTop * zoom)
						$('#canvas-passepartout').css('border-bottom-width', passepartoutBottom * zoom)
						$('#canvas-passepartout').css('border-left-width', passepartoutLeft * zoom)
						$('#canvas-passepartout').css('border-right-width', passepartoutRight * zoom)

						$('#canvas-passepartout').css('border-color', passepartoutColor)

						//hidden copy
						$('#canvas-border-hidden').css('width', width + 'px')
						$('#canvas-border-hidden').css('height', height + 'px')

						$('#canvas-border-hidden')
							.css('padding-top', passepartoutTop * zoom )
						$('#canvas-border-hidden')
							.css('padding-bottom', passepartoutBottom * zoom )
						$('#canvas-border-hidden')
							.css('padding-left', passepartoutLeft * zoom )
						$('#canvas-border-hidden')
							.css('padding-right', passepartoutRight * zoom )

						$('#canvas-border-hidden').css('background-color', passepartoutColor)

						replaceGallery()
						saveImage()
 					}
 				}

 				function saveImage() {
 					var imageWrapper = $('#canvas-border-hidden')

 					$('.single_add_to_cart_button').addClass('loading-cut')

 					domtoimage.toPng(imageWrapper.get(0))
				    .then(function(blob) {

				    	$('.single_add_to_cart_button').removeClass('loading-cut')

				      	var files = [
						  dataURLtoFile(blob, 'image_cropped.png')
						];

						$("input[name='bild_beskuren']").prop("files", new FileListItem(files));
				    });
 				}

				function FileListItem(a) {
				  a = [].slice.call(Array.isArray(a) ? a : arguments)
				  for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
				  if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
				  for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
				  return b.files
				}

				function dataURLtoFile(dataurl, filename) {
 
			        var arr = dataurl.split(','),
			            mime = arr[0].match(/:(.*?);/)[1],
			            bstr = atob(arr[1]), 
			            n = bstr.length, 
			            u8arr = new Uint8Array(n);
			            
			        while(n--){
			            u8arr[n] = bstr.charCodeAt(n);
			        }
			        
			        return new File([u8arr], filename, {type:mime});
			    }

			    function setInputVal (input, val) {
			    	//$("input.wccpf-field[name='" + input + "']").val(val)
			    }

				function setImageMaxSize () {
			    	if(pxWidth && pxHeight && dpi) {
			    		console.log(pxWidth, pxHeight, dpi)


			    		var dpmm = 25.4 / dpi
			    		maxWidth = Math.floor(pxWidth * dpmm)
			    		maxHeight = Math.floor(pxHeight * dpmm)

			    		console.log(dpmm, maxWidth, maxHeight)

			    		//300 DPI = 11.811 dpmm 

			    		frameInnerWidth = maxWidth
 						frameInnerHeight = maxHeight

			    		setInputVal('frame_outer_width', maxWidth)
			        	setInputVal('frame_outer_height', maxHeight)

			    		setInputVal('max_print_size', maxWidth + 'mm x ' + maxHeight + 'mm')
			    	}
			    }			    

			    // Setup

			    // load img
				$(".js-custom-image input").change(() => {
					var input = $(".js-custom-image input")[0]

					if(input && input.files && input.files[0]) {

						var img = input.files[0]
						var imgObj = new Image();
						var _URL = window.URL || window.webkitURL;
				        var objectUrl = _URL.createObjectURL(img);
				        imgObj.onload = function () {

				        	pxWidth = this.width
				        	pxHeight = this.height
				        	pxRatio = this.height / this.width

				        	//setImageMaxSize()

				            _URL.revokeObjectURL(objectUrl);
				            loadCrop()
				        };
				        imgObj.src = objectUrl;



					}
				});

				//Set frame size
				$(".js-frame-width input, .js-frame-height input").change(() => {
					setArea()
				});
				
				function setArea() {
					frameOuterWidth = $(".js-frame-width input").val()
					frameOuterHeight = $(".js-frame-height input").val()

					frameInnerWidth = frameOuterWidth
 					frameInnerHeight = frameOuterHeight

					if(frameInnerWidth && frameInnerHeight) {
						loadCrop()
					}
				}

				$(".js-passepartout-select select").change(() => {

					if($(".js-passepartout-select select").val() !== 'ingen') {
						passepartoutTop = $(".js-passepartout-top input").val()
						passepartoutBottom = $(".js-passepartout-bottom input").val()
						passepartoutLeft = $(".js-passepartout-left input").val()
						passepartoutRight = $(".js-passepartout-right input").val()
					} else {
						passepartoutTop = 0
						passepartoutBottom = 0
						passepartoutLeft = 0
						passepartoutRight = 0
					}

					loadPreview()
				});

				/*$("select.wccpf-field[name^='passepartout_']").change(() => {
					passepartoutColor = $("select.wccpf-field[name='passepartout_color']").val()

					loadPreview()
				});*/

				$('#croppie').on('update.croppie', (ev, cropData)=> {
					$('#croppie').croppie('result', 'base64').then(function(img) {
					    $('#canvas').css('background-image', 'url(' + img + ')')
					    $('#canvas-hidden').css('background-image', 'url(' + img + ')')

					    saveImage()
					});
				});
				
			});

		  
		</script>

		<?php
	}

?>