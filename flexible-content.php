<?php

// Check value exists.
if( have_rows('flexibelt_innehall') ):
    while ( have_rows('flexibelt_innehall') ) : the_row();
        
        
        if( get_row_layout() == 'produktblock' ): ?>
            <?php
                // Variabler
                $featured_posts = get_sub_field('produkter');
                $rubrik = get_sub_field('rubrik');

                // Code
            ?>
            <section class="section flexible-content products">
                <?php if( $rubrik ): ?>
                    <h2 class="block-title"><?php echo $rubrik ?></h2>
                <?php endif;

                if( $featured_posts ): ?>
                    <ul class="products-wrapper">
                        <?php 

                        foreach( $featured_posts as $featured_post ):
                            
                            $global_product = new WC_Product( $featured_post->ID );
                            $product = wc_get_product($featured_post->ID);

                            $permalink = get_permalink( $featured_post->ID );
                            $title = get_the_title( $featured_post->ID );
                            $excerpt = get_the_excerpt( $featured_post->ID );
                            $post_content = get_post_field('post_content', $featured_post->ID);
                            $lang_produktbeskrivning = get_field( 'lang_produktbeskrivning', $featured_post->ID );
                            $nyhet = get_field( 'nyhet', $featured_post->ID );
                            $post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_post->ID ), 'medium' );
                            // $post_image_alt = 

                            ?>
                            <li class="product-item">
                                <div class="product-item-inner">
                                    <div class="image">
                                        <?php if( $nyhet ): ?>
                                            <div class="nyhet"><?php echo $nyhet; ?></div>
                                        <?php endif; ?>

                                        <img src="<?php echo $post_image[0]; ?>" alt="hej">

                                    </div>
                                    <div class="product-meta-wrapper">
                                        <div class="title">
                                            <h5>
                                                <?php echo $title; ?>
                                            </h5>
                                        </div>
                                        <div class="excerpt">
                                            <p>
                                                <?php echo $excerpt; ?>
                                            </p>
                                        </div>
                                        <div class="card-footer">
                                            <div class="price">
                                                <?php echo $product->get_price(); ?> SEK
                                                
                                            </div>
                                        
                                            <div class="buttons-wrapper full-width">
                                                <a href="<?php echo $permalink; ?>" class="button primary">
                                                    <span>Läs mer</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </section>
            <?php

        
        elseif( get_row_layout() == 'textblock' ):
            // Variabler
            $textblock = get_sub_field('textblock');
            ?>
                <section class="section flexible-content content">
                    <div class="content-wrapper">
                        <?php echo $textblock; ?>
                    </div>
                </section>
            <?php


        elseif( get_row_layout() == 'linkblock' ):
            // Variabler
            $links = get_sub_field('links');
            ?>
                <section class="section flexible-content linkblock">
                    <div class="links-wrapper">    
                        <?php
                            foreach($links as $link) {
                                ?>
                                    <a class="link-item" target="<?php echo $link['link']['target']; ?>" href="<?php echo $link['link']['url']; ?>">
                                        
                                        <?php if( $link['image'] ) : ?>
                                            <img src="<?php echo $link['image']['sizes']['large']; ?>" alt="<?php echo $link['kategori_bild']['alt']; ?>">
                                        <?php endif; ?>

                                        <?php if( $link['link']['title'] ) : ?>
                                            <h3><?php echo $link['link']['title']; ?></h3>
                                        <?php endif; ?>

                                        <?php if( $link['text'] ) : ?>
                                            <p><?php echo $link['text']; ?></p>
                                        <?php endif; ?>

                                        <div class="buttons-wrapper full-width">
                                            <span class="button primary">Läs mer</span>
                                        </div>
                                    </a>
                                <?php
                            }
                        ?>
                    </div>
                </section>
            <?php


        endif;
    endwhile;

else :
    
endif;

?>