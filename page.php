<?php get_header(); ?>
<div class="big-wrapper">
	<div class="wrapper-inner">
		<div class="wrapper-inner-inner">

			<?php if ( is_page( array( 'varukorg', 'kassan' ) ) ) {
				while ( have_posts() ) : the_post(); ?>
				<div class="page-content">
					<?php
						the_content();
					endwhile;
					?>
				</div>
				<?php
			} else {
				?>
					
					<div class="sub-page-hero-wrapper">
						
						<div class="text-wrapper">
							<div class="text-wrapper-inner">
								<div class="title">
									<h1><?php the_title(); ?></h1>
								</div>
								<p class="description">
									<?php
										// verify that this is a product category page
										if ( is_product_category() ){
											global $wp_query;
											$cat = $wp_query->get_queried_object();
											$description = $cat->description;
											echo $description;
										}

										$page_fields = get_field('page_fields');
										echo "<div class='content'>";
											echo $page_fields['text'];
											?>
												<?php if($page_fields['link']) : ?>
													<div class="buttons-wrapper auto-width">
														<a class="button primary" target="<?php echo $page_fields['link']['target']; ?>" href="<?php echo $page_fields['link']['url']; ?>">
															<?php echo $page_fields['link']['title']; ?>
														</a>
													</div>
												<?php endif; ?>
											<?php
										echo "</div>";
									?>
								</p>
							</div>
						</div>

						<div class="image-wrapper">
							<?php
							// verify that this is a product category page
							if ( is_product_category() ){
								global $wp_query;
								$cat = $wp_query->get_queried_object();
								$alt = $cat->name;
								$thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true ); 
								$image = wp_get_attachment_url( $thumbnail_id ); 
								echo "<img src='{$image}' alt='$alt' />";
							}

							the_post_thumbnail();

							?>

						</div>
					
					</div>

					<?php while ( have_posts() ) : the_post(); ?>
					<div class="page-content">
						<?php
							the_content();
						endwhile;
						?>
					</div>
					<?php include("flexible-content.php"); ?>
				<?php
			} ?>
			
		</div>
	</div>	
</div>
<?php get_footer(); ?>