<?php get_header(); ?>
<div class="big-wrapper">
	<div class="wrapper-inner">
		<div class="wrapper-inner-inner">
			<div class="page-hero-wrapper">
				<?php
					$post_thumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
				?>
				<div class="page-hero-image" style="background: url('<?php echo $post_thumbnail; ?>'); background-size: cover; background-position: center;">
					<div class="page-hero-content">
						<div class="content-wrapper">
							<h2>Tavelramar med udda mått - kvalité till låga priser</h2>
							<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Consequuntur eum eveniet repellendus, adipisci maiores voluptas eius nihil, autem quisquam ab obcaecati sit, similique sint rem cumque eaque rerum. Ab, in.</p>
							<div class="buttons-wrapper auto-width center">
								<div class="button primary">
									En knapp
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include("flexible-content.php"); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>