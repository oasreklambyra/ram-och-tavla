<?php get_header(); ?>
<div class="big-wrapper">
	<div class="wrapper-inner">

		<div class="wrapper-inner-inner">
			<?php while ( have_posts() ) : the_post(); ?>
				<!-- <h1><?php the_title(); ?></h1> -->
				<?php the_post_thumbnail('page-hero'); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>