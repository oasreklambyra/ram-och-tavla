<footer>
    <div class="footer-wrapper">
        <!-- <div class="footer-column">
            <?php the_field('footer_column_1','options'); ?>
        </div>
        <div class="footer-column">
            <?php the_field('footer_column_2','options'); ?>
        </div>
        <div class="footer-column">
            <?php the_field('footer_column_3','options'); ?>
        </div>
        <div class="footer-column">
            <img src="<?php the_field('footer_column_4','options'); ?>" alt="klarna payments">
        </div> -->

        <div class="footer-inner">

            <h5>RAMOTAVLA.SE</h5>

            <hr>
            
            <div class="content">
                <p>FRÖSETS RAMVERKSTAD AB</p>
                <p>ALFAVÄGEN 5</p>
                <p>55652 JÖNKÖPING</p>
                <p>KUNDSERVICE@RAMOTAVLA.SE</p>
                <p>036-336 68 06</p>
            </div>

            <hr>

            <div class="content">
                <p>Villkor & info</p>
                <p>Snabb tillverkning och leverans</p>
                <p>7-10 arbetsdagar från oss med DHL Servicepoint</p>
            </div>
        </div>
        
        
    </div>
</footer>
</body>
</html>
<?php wp_footer(); ?>