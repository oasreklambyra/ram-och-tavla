<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ram och tavla</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <?php wp_head(); ?>
    <style>
        .button.primary span,
.button.single_add_to_cart_button span,
.button.add_to_cart_button span,
.button.wc-forward span,
.woocommerce button.button.primary span,
.woocommerce button.button.single_add_to_cart_button span,
.woocommerce button.button.add_to_cart_button span,
.woocommerce button.button.wc-forward span,
.woocommerce a.button.primary span,
.woocommerce a.button.single_add_to_cart_button span,
.woocommerce a.button.add_to_cart_button span,
.woocommerce a.button.wc-forward span,
input.primary span,
input.single_add_to_cart_button span,
input.add_to_cart_button span,
input.wc-forward span {
  color: white !important;
}
    </style>
</head>
<header>
    <img src="<?php bloginfo('template_url'); ?>/assets/images/mobile-logo.svg" alt="mobile-logo">
    <div class="right">
        <div class="burger-wrapper">
            <span class="line"></span>
            <span class="line"></span>
            <span class="line"></span>
        </div>
        <div class="search-wrapper">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g>
                    <g>
                        <path d="M225.474,0C101.151,0,0,101.151,0,225.474c0,124.33,101.151,225.474,225.474,225.474
                            c124.33,0,225.474-101.144,225.474-225.474C450.948,101.151,349.804,0,225.474,0z M225.474,409.323
                            c-101.373,0-183.848-82.475-183.848-183.848S124.101,41.626,225.474,41.626s183.848,82.475,183.848,183.848
                            S326.847,409.323,225.474,409.323z"/>
                    </g>
                </g>
                <g>
                    <g>
                        <path d="M505.902,476.472L386.574,357.144c-8.131-8.131-21.299-8.131-29.43,0c-8.131,8.124-8.131,21.306,0,29.43l119.328,119.328
                        c4.065,4.065,9.387,6.098,14.715,6.098c5.321,0,10.649-2.033,14.715-6.098C514.033,497.778,514.033,484.596,505.902,476.472z"/>
                </g>
            </svg>
        </div>
    </div>
</header>
<div class="meny-wrapper">
    <div class="meny-wrapper-inner">
         <div class="logo">
            <a href="/">
                <img src="<?php the_field('logotype','options'); ?>" alt="ram & tavla logo">
            </a>
        </div>

        <div class="menu-inner">
            <h4>Meny</h4>

            <ul class="menu-primary">
                <?php
                    if( have_rows('menu', 'options') ):
                        while( have_rows('menu', 'options') ) : the_row();
                            $link = get_sub_field('link','options');
                            if( $link ):
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <li>
                                    <a class="text_small" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                        <?php echo $link_title ?>
                                    </a>
                                </li>
                            <?php endif;
                        endwhile;
                    else :
                    endif;
                ?>
            </ul>

            <ul class="menu-sub">
                <?php
                    if( have_rows('menu_sub', 'options') ):
                        while( have_rows('menu_sub', 'options') ) : the_row();
                            $link = get_sub_field('link','options');
                            if( $link ):
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <li>
                                    <a class="text_small" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                        <?php echo $link_title ?>
                                    </a>
                                </li>
                            <?php endif;
                        endwhile;
                    else :
                    endif;
                ?>
            </ul>
        </div>

        <div class="cart-wrapper">
            <a href="/varukorg/" class="input-wrapper">
                <div class="icon-wrapper">
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-cart" class="svg-inline--fa fa-shopping-cart fa-w-18" role="img" viewBox="0 0 576 512"><path fill="currentColor" d="M528.12 301.319l47.273-208C578.806 78.301 567.391 64 551.99 64H159.208l-9.166-44.81C147.758 8.021 137.93 0 126.529 0H24C10.745 0 0 10.745 0 24v16c0 13.255 10.745 24 24 24h69.883l70.248 343.435C147.325 417.1 136 435.222 136 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-15.674-6.447-29.835-16.824-40h209.647C430.447 426.165 424 440.326 424 456c0 30.928 25.072 56 56 56s56-25.072 56-56c0-22.172-12.888-41.332-31.579-50.405l5.517-24.276c3.413-15.018-8.002-29.319-23.403-29.319H218.117l-6.545-32h293.145c11.206 0 20.92-7.754 23.403-18.681z"/></svg>
                </div>
                <div class="text-wrapper">
                    <div>Varukorg</div>
                    (<div id="mini-cart-count"></div>)
                </div>

            </a>
        </div>
    </div>
</div>
<div class="search-result-wrapper">
                <div id="datafetch">
                    <div id="loading-search">
                        <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                        <style>
                            .lds-ring {
                                display: inline-block;
                                position: absolute;
                                top: 50%;
                                margin-top: -40px;
                                width: 80px;
                                height: 80px;
                                }
                                .lds-ring div {
                                box-sizing: border-box;
                                display: block;
                                position: absolute;
                                width: 64px;
                                height: 64px;
                                margin: 8px;
                                border: 8px solid #111;
                                border-radius: 50%;
                                animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
                                border-color: #111 transparent transparent transparent;
                                }
                                .lds-ring div:nth-child(1) {
                                animation-delay: -0.45s;
                                }
                                .lds-ring div:nth-child(2) {
                                animation-delay: -0.3s;
                                }
                                .lds-ring div:nth-child(3) {
                                animation-delay: -0.15s;
                                }
                                @keyframes lds-ring {
                                0% {
                                    transform: rotate(0deg);
                                }
                                100% {
                                    transform: rotate(360deg);
                                }
                            }
                        </style>
                    </div>

                    <div class="search-wrapper-big">
                        <h2>Sök efter artiklar</h2>

                        <input type="text" name="keyword" id="keyword" placeholder="Sök" onkeyup="fetch()"></input>

                        <div class="datafetch-inner">

                        </div>
                        <span class="spinner"></span>
                    </div>
                </div>
            </div>
<body <?php body_class(); ?>>