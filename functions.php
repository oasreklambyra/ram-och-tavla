<?php

include 'image-configurator.php';

/*
    Functions right here

    #1 Load stylesheet
    #2 Load scripts
    #3 Add support
    #4 Add image sizes
    #5 Cart
    #5 Search

*/

// #1 Load stylesheet
function load_stylesheets()
{
    wp_register_style('stylesheet', get_template_directory_uri() . '/dist/styles/styles.css', '', 1, 'all');
    wp_enqueue_style('stylesheet');
}

add_action('wp_enqueue_scripts', 'load_stylesheets');


// #2 Load scripts
function load_javascripts()
{
    //wp_register_script('functions', get_template_directory_uri() . '/dist/scripts/functions-min.js', 'jquery', 1, true);
    wp_register_script('functions', get_template_directory_uri() . '/assets/scripts/functions.js', 'jquery', 1, true);
    wp_enqueue_script('functions');
}
add_action('wp_enqueue_scripts', 'load_javascripts');


// #3 Add support
add_theme_support('post-thumbnails');


// #4 Add image sizes
add_image_size('page-hero', 1440, 800, false);
add_image_size('product-image', 800, 800, false);


// #5 Cart
add_filter( 'woocommerce_add_to_cart_fragments', 'wc_refresh_mini_cart_count');
function wc_refresh_mini_cart_count( $fragments ){
    ob_start();
    ?>
    <div id="mini-cart-count">
        <?php echo WC()->cart->get_cart_contents_count(); ?>
    </div>
    <?php
        $fragments['#mini-cart-count'] = ob_get_clean();
    return $fragments;
}


// #5 Search
add_action( 'wp_footer', 'ajax_fetch' );
function ajax_fetch() {
?>
<script type="text/javascript">
function fetch(){

    var keyword = jQuery('#keyword').val()
    if(keyword == "") {
        jQuery('#datafetch .datafetch-inner').html("");
    } else {
        jQuery.ajax({
            url: '<?php echo admin_url('admin-ajax.php'); ?>',
            type: 'post',
            data: {
                action: 'data_fetch',
                keyword: keyword
            },
            beforeSend: function(){
                jQuery('#loading-search').removeClass('hide');
            },
            success: function(data) {
                jQuery('#datafetch .datafetch-inner').html( data );
                jQuery('#loading-search').addClass('hide');
            }
        });
    }
}
</script>

<?php
}

add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
function data_fetch(){

    $the_query = new WP_Query( array(
        'posts_per_page' => -1,
        's' => esc_attr( $_POST['keyword'] ),
        'post_type' => 'product'
    ) );

    if( $the_query->have_posts() ) :
        while( $the_query->have_posts() ): $the_query->the_post(); ?>

            <a class="search-result-item" href="<?php echo esc_url( post_permalink() ); ?>">
                <?php the_post_thumbnail('thumbnail'); ?>
                <div class="meta">
                    <h3><?php the_title();?></h3>
                </div>
            </a>

        <?php endwhile;
        wp_reset_postdata();
    else:
        echo "<div class='no-results-wrapper'>";
        echo "<h2>";
            echo 'Hittar inga resultat';
        echo "</h2>";
        echo "<p style='width:100%;text-align:center;'>";
            echo "Testa att söka på något annat...";
        echo "</p>";
        echo "</div>";
    endif;
        die();
}

if(function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

add_filter( 'woocommerce_product_add_to_cart_text', function( $text ) {
	global $product;
	if ( $product ) {
		$text = $product->is_purchasable() ? __( 'Läs mer', 'woocommerce' ) : __( 'Läs mer', 'woocommerce' );
	}
	return $text;
}, 10 );

remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 20);

// add_action('wp_ajax_ql_woocommerce_ajax_add_to_cart', 'ql_woocommerce_ajax_add_to_cart');

// add_action('wp_ajax_nopriv_ql_woocommerce_ajax_add_to_cart', 'ql_woocommerce_ajax_add_to_cart');

// function ql_woocommerce_ajax_add_to_cart() {
//     $product_id = apply_filters('ql_woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
//     $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
//     $variation_id = absint($_POST['variation_id']);
//     $passed_validation = apply_filters('ql_woocommerce_add_to_cart_validation', true, $product_id, $quantity);
//     $product_status = get_post_status($product_id);

//     if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {
//         do_action('ql_woocommerce_ajax_added_to_cart', $product_id);
//         if ('yes' === get_option('ql_woocommerce_cart_redirect_after_add')) {
//             wc_add_to_cart_message(array($product_id => $quantity), true);
//         }
//         WC_AJAX :: get_refreshed_fragments();
//     } else {
//         $data = array(
//             'error' => true,
//             'product_url' => apply_filters('ql_woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));
//         echo wp_send_json($data);
//     }
//     wp_die();
// }