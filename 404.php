<div class="big-wrapper">
	<?php get_header(); ?>

		<div class="wrapper-inner">

			<div class="wrapper-inner-inner">

                <h1>404</h1>
                
                <p>
                    Ledsen, sidan <span><?php echo $_SERVER['REQUEST_URI']; ?>/</span> hittades inte.
                </p>
				
			</div>
		</div>

	<?php get_footer(); ?>
</div>